import React, { Component } from 'react';
import LoginBanner from '../components/app/auth/loginbanner';
import LoginForm from '../components/app/auth/loginform';

export default class AuthPage extends Component<Props> {
    props: Props;
  
    render() {
      return (
        <div className="login-page">
          <LoginBanner />
          <LoginForm />
        </div>
      );
    }
  }