import React from 'react'

type   Props = {};

export default class Avatar extends React.Component<Props> {
  props: Props;

  render() {
    return(
      <div className="avatar">
        <img src="src" alt="" className="avatar___image" />
        <div className="avatar__image avatar__image__text">
          Avatar
        </div>
      </div>
    );
  }

}