import React from 'react';
import styles from './sidebar.css'

type Props = {};
const sidebar = [
  {
    text: "Ledger Group",
    to: "/accounts/ledger-group",
    icon: "fa fa-table"
  },
  {
    text: "Ledger",
    to: "/accounts/ledger",
    icon: "fa fa-th"
  },
  {
    text: "Accounts Entry",
    to: "/accounts/accountsEntry",
    icon: "fa fa-calendar"
  },
  { text: "Cash Book", to: "/accounts/cash-book", icon: "fa fa-cog" },
  { text: "Day Book", to: "/accounts/day-book", icon: "fa fa-cog" },
  {
    text: "Balance Sheet",
    to: "/accounts/balance-sheet",
    icon: "fa fa-table"
  }
]

export default class SideBar extends React.Component<Props> {
  props: Props;

  render() {
    return(
      <div className={styles.sidebar}>
        <div>
          <div className={styles.sidebar__header}>
            <ul>
              {sidebar.map((list) => {
                return(
                  <li>
                    <i className={list.icon} />{list.text}
                  </li>
                )}
              )}
            </ul>
          </div>
        </div>
      </div>
    );
  }
}