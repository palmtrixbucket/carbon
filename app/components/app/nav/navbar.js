import React from 'react';
import { Link } from 'react-router-dom'
import styles from './nav.css';
import { list } from 'postcss';

const modules = [
  {
    text: 'Inventory',
    icon: '../internals/svg/document.svg',
    to: '/inventory'
  },
  {
    text: 'Accounts',
    icon: '../internals/svg/department.svg',
    to: '/accounts'
  },
  { text: 'Reports', icon: '../internals/svg/document.svg', to: '/reports' },
  { text: 'Settings', icon: '../internals/svg/cog.svg', to: '/settings' }
];

export default class NavBar extends React.Component {
  
  render() {
    return(
      <div className={styles.header}>
        <section className={styles.header__section}>
          <div className={styles.app_info}>
            <div className={styles.logo}>
              <i className="fa fa-th" />
            </div>
            <div className={styles.app_name}>
              <Link to="">Carbon</Link>
            </div>
          </div>
          <div className="stretch" />
          <div className={styles.profile_name}>
            User Name
          </div>
          <div>
            <a className={styles.logout__link}>
              <i className="fa fa-lock" /> Logout
            </a>
          </div>
        </section>
        <nav className={styles.navbar} />
      </div>
    );
  }
}