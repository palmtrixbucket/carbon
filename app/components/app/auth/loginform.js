import React from 'react'
import { Link } from 'react-router-dom'
import styles from './auth.css'

type Props = {};

export default class LoginForm extends React.Component<Props> {
  props: Props;

  render(){
    return(
      <div className={styles.login_form}>
        <div className={styles.circles} />
        <div className={styles.login_form_content}>
          <h1>Sign In</h1>
          <p>Enter Your Details </p>
          <div className={styles.login_form__input}>
            <label>Email Address:</label>
            <input onChange={() => {}} type="text" placeholder="name@domain.com" />
          </div>
          <div className={styles.login_form__input}>
            <label>Password:</label>
            <input onChange={() => {}} type="password" placeholder="enter your password" />
          </div>  
          <div className={styles.login_form__input} style={{ display: 'flex' }}>
            <input type="checkbox" id="remember" className={styles.checkbox} />
            <label htmlFor="remember">
              <div className={styles.checkbox__checkmark} />
              <span>Remember Password</span>
            </label>
            <div className={styles.forgot_link}>
              <a href="">Forgot Password?</a>
            </div>
          </div>
          <div className={styles.login_form.login_input}>
            <div className={styles.login__button_ct}>
              <button className={styles.login__button}>
                <Link to='/home' className={styles.login__link}>Sign In</Link>
              </button>
            </div>
          </div>
        </div>  
      </div>    
    );
  }
}