// @flow
import React from 'react';
import styles from './auth.css'

type Props = {};

export default class LoginBanner extends React.Component<Props> {
  props: Props;

  render() {
    return(
      <div className={styles.login_banner}>
        <div className={styles.logo}>
          <div className={styles.logo_letter}>C</div>
          <div className={styles.logo_text}>Carbon</div>
        </div>
      </div>
    );
  }
}
