// @flow
import React, { Component } from 'react';
import NavBar from './app/nav/navbar';
import SideBar from './app/sidebar/sidebar';

type Props = {};

export default class Home extends Component<Props> {
  props: Props;

  render() {
    return (
      <div>
        <NavBar />
        <SideBar />
      </div>
    );
  }
}
